import java.sql.*;

public class Main1 {
    private static void defineTables(Connection c) throws SQLException {
        /** Define table book(_id_, title, comment) using c.createStatement() and statement.execute()
         * @see java.sql.Statement */
        //TODO: Create stmt and execute it
        Statement stmt = c.createStatement();
        stmt.execute("DROP TABLE book; CREATE TABLE book (id SERIAL PRIMARY KEY, title VARCHAR(16) NOT NULL);");
    }

    private static void insertBooks(Connection c) throws SQLException {
        /** Insert books 'Proces' and 'Zámek' to book using c.createStatement() and statement.execute()
         * @see java.sql.Statement */
        Statement stmt = c.createStatement();
        stmt.execute("INSERT INTO book (title) VALUES ('Zámek'), ('Proces');");

    }

    private static void queryBooks(Connection c) throws SQLException {
        /** SELECT all gooks using c.createStatement(), statement.executeQuery() and ResultSet iteration
         * @see java.sql.ResultSet */
        Statement s3 = c.createStatement();
        ResultSet rs = s3.executeQuery("SELECT id, title FROM book");
        while (rs.next()) {
            System.out.println("Id: " + rs.getInt("id") + " | title: " + rs.getString(2));
        }
    }

    private static void retrieveBook(Connection c, String title) throws SQLException {
        /** SELECT a single book of arbitrary title from c using a PreparedStatement
         * @see java.sql.PreparedStatement */

        PreparedStatement ps = c.prepareStatement("SELECT * FROM book WHERE (title = ?)");
        ps.setString(1, title);
        ResultSet rs = ps.executeQuery();
        System.out.println("Search Results for " + title);
        while (rs.next()) {
            System.out.println("Id: " + rs.getInt("id") + " | title: " + rs.getString(2));
        }
    }


    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Connection c = DriverManager.getConnection(
                "jdbc:postgresql://slon.felk.cvut.cz:5432/ullriher",
                "ullriher",
                "DBS2022");
        defineTables(c);
        insertBooks(c);
        queryBooks(c);
        retrieveBook(c, "Zámek");
        c.close();
    }
}