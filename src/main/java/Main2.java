import javax.persistence.*;
import java.util.List;

public class Main2 {
    public static EntityManager createEntityManager() {
        /** Create EntityManager using persistence unit named (by default) ApplicationPU using EntityManagerFactory
         * @see javax.persistence.EntityManagerFactory */
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ApplicationPU");
        EntityManager em = emf.createEntityManager();
        return em;
    }

    private static Book retrieveBook(EntityManager em, Integer id) {
        /** SELECT a single book of arbitrary ID from c using em.find()
         * @see javax.persistence.EntityManager */
        Book b = em.find(Book.class, id);
        System.out.println("Retrieved a book " + b.toString());
        return b;
    }

    private static void queryBooks(EntityManager em) {
        /** SELECT all books using TypedQuery<Book> and print them to console
         * @see javax.persistence.TypedQuery */
        TypedQuery<Book> q2 = em.createQuery("SELECT b FROM Book AS b", Book.class);
        List<Book> l = q2.getResultList();
        System.out.println("SELECTed books:");
        for (Book b : l) {
            System.out.println(b);
        }
    }

    private static Book retrieveBookByTitle(EntityManager em, String title) {
        /** SELECT a single book of arbitrary title from c using parametrized query
         * @see javax.persistence.EntityManager */
        TypedQuery<Book> q3 = em.createQuery("SELECT b FROM Book AS b WHERE (b.title = :BookTitle)", Book.class);
        q3.setParameter("BookTitle", title);
        Book b = q3.getSingleResult();
        System.out.println("Book " + b.toString() + " retrieved by title.");
        return b;
    }


    private static Book createBook(EntityManager em, String title) {
        /** Insert a book with given title using em.persist(book) and return it - do so within a transaction
         * @see javax.persistence.EntityTransaction
         * @see javax.persistence.EntityManager */
        EntityTransaction t = em.getTransaction();
        t.begin();
        Book book = new Book();
        book.setTitle(title);
        em.persist(book);
        t.commit();
        System.out.println("insertBook(title=" + title + ") created " + book);
        return book;
    }

    private static void updateBook(EntityManager em, Book book, String title) {
        /** Change the title of the given book - do so within a transaction
         * @see javax.persistence.EntityTransaction
         * @see javax.persistence.EntityManager */
        EntityTransaction t = em.getTransaction();
        t.begin();
        book.setTitle(title);
        em.persist(book);
        t.commit();
        System.out.println("changeTitle(title=" + title + ") altered " + book);
    }

    private static void removeBook(EntityManager em, Book book) {
        /** Remove the given book - do so within a transaction
         * @see javax.persistence.EntityTransaction
         * @see javax.persistence.EntityManager */
        EntityTransaction t = em.getTransaction();
        t.begin();
        em.remove(book);
        em.persist(book);
        t.commit();
        System.out.println("removeBook(book=" + book.toString() + ") removed " + book);
    }


    public static void main(String[] args) throws ClassNotFoundException {
        Class c = Class.forName("org.eclipse.persistence.jpa.PersistenceProvider");
        EntityManager em = createEntityManager();
        Book book = retrieveBook(em, 1);
        queryBooks(em);
        Book book2 = retrieveBookByTitle(em, "Zámek");
        Book book3 = createBook(em, "Proměna");
        updateBook(em, book3, "Amerika");
        removeBook(em, book3);
        em.close();
    }
}